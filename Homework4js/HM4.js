let num1 = parseFloat(prompt("Введіть перше число:"));
let num2 = parseFloat(prompt("Введіть друге число:"));
let operator = prompt("Введіть математичну операцію (+, -, *, /):");


function calculate(a, b, op) {
    switch (op) {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            if (b !== 0) {
                return a / b;
            } else {
                return "На нуль дулити не можна!";
            }
        default:
            return "Невірна операція!";
    }
}



let result = calculate(num1, num2, operator);
console.log(result);
