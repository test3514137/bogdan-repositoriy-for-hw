import React, {useState} from 'react'
import Products from "./components/Products/Products.jsx";
import Header from "./components/Header/Header.jsx";
import './App.scss'

function App() {

    const [favoriteProducts, setFavoriteProducts] = useState([])
    const [cartProducts, setCartProducts] = useState([])

    const handleFavoriteProducts = (item) => {
        if (favoriteProducts.length >= 1) {
            let isProductAdded = favoriteProducts.find((product) => product.id === item.id)
            if (!isProductAdded) {
                setFavoriteProducts((prevState) => [...prevState, item])
                localStorage.setItem("favoriteProducts", JSON.stringify([...favoriteProducts, item]));
            }
            if (isProductAdded) {
                let indexOfAddedProduct = favoriteProducts.indexOf(item)
                let changedFavoriteProducts = favoriteProducts.splice(indexOfAddedProduct, 1)
                setFavoriteProducts((changedFavoriteProducts) => [...changedFavoriteProducts])
                localStorage.removeItem("favoriteProducts");
                localStorage.setItem("favoriteProducts", JSON.stringify([...favoriteProducts]));
            }
        } else {
            setFavoriteProducts((prevState) => [...prevState, item])
            localStorage.setItem("favoriteProducts", JSON.stringify([...favoriteProducts, item]));
        }
    }

  

    const handleCartProducts = (item) => {
            setCartProducts((prevState) => [...prevState, item])
            localStorage.setItem("cartProducts", JSON.stringify([...cartProducts, item]));
    }

  return (
      <>
          <Header
              favoriteProductsCounter={favoriteProducts.length}
              cartProductsCounter={cartProducts.length}
          />
          <Products
              onFavorite={handleFavoriteProducts}
              onCart={handleCartProducts}
          />
      </>
  )
}

export default App
