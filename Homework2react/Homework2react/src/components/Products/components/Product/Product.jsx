import React, { useState } from 'react';
import Button from "../../../Button/Button.jsx";
import FavoritesIcon from "../../../ui/icons/FavoritesIcon.jsx";

import cn from 'classnames'
import "./Product.scss"
import PropTypes from "prop-types";

const Product = (props) => {

    const {
        id,
        name,
        price,
        picture,
        color,
        onClick,
        onFavorite,
    } = props

    const [isFavorite, setIsFavorite] = useState(false)

    const handleFavorite = () => {
        setIsFavorite(!isFavorite)
    }

    return (
        <div className="product">
            <div className="product-icons-wrapper" onClick={() => {
                handleFavorite()
                onFavorite()
            }
            }
            >
                <FavoritesIcon className={cn("product-icon", { "is-favorite": isFavorite })} />
            </div>
            <div className="product-image">
                <img
                    src={picture}
                    alt={name} />
            </div>
            <div className="product-info">
                <h3 className="product-name">{name}</h3>
                <span className="product-color">{`color: ${color}`}</span>
                <span className="product-ia">{`id: ${id}`}</span>
            </div>
            <div className="btn-wrapper">
                <Button type="button" className="product-btn" onClick={onClick} text="Add to cart"></Button>
                <p className="product-price">{`$${price}`}</p>
            </div>
        </div>
    );
};

Product.propTypes = {
    id: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    picture: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    onFavorite: PropTypes.func,

}

export default Product;