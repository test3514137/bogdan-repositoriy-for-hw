import React from 'react';
import Products from "../../components/Products/Products.jsx";

const Homepage = ({ onFavorite, onCart, favoriteProducts, cartProducts, currentProduct, handleCurrentProduct, handleModal }) => {
    return (
        <div>
            <Products
                onFavorite={onFavorite}
                onCart={onCart}
                favoriteProducts={favoriteProducts}
                cartProducts={cartProducts}
                handleCurrentProduct={handleCurrentProduct}
                currentProduct={currentProduct}
                handleModal={handleModal}
            />
        </div>
    );
};

export default Homepage;