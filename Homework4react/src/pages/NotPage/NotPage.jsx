import React from 'react';
import { Link } from 'react-router-dom';
import Homepage from "../HomePage/Homepage.jsx";


const NotPage = () => {
    return (
        <div>
            Error 404. Page not found. Please, visit <Link to="/">Home Page</Link>.
        </div>
    );
};

export default NotPage;