// import React, { useState } from 'react';
// import Product from "../../components/Products/components/Product/Product.jsx";
// import ProductModalAddToCart from "../../components/Products/components/ProductModals/ProductModalAddToCart.jsx";

// import "./CardPage.scss"

// const CartPage = ({ cartProducts, handleCurrentProduct, currentProduct, favoriteProducts, onFavorite, onCart }) => {

//     const [isModal, setIsModal] = useState(false)
//     const handleModal = () => setIsModal(!isModal)

//     return (
//         <>
//             <div className="cart-products-wrapper">
//                 {cartProducts?.map((cartProduct) => <Product
//                     key={cartProduct.id}
//                     id={cartProduct.id}
//                     name={cartProduct.name}
//                     price={cartProduct.price}
//                     picture={cartProduct.picture}
//                     color={cartProduct.color}
//                     linkPath={`/product/${cartProduct.id}`}
//                     onClick={() => {
//                         handleCurrentProduct(cartProduct)
//                         handleModal()
//                     }
//                     }
//                     onFavorite={() => {
//                         onFavorite(cartProduct)
//                     }
//                     }
//                     favoriteProducts={favoriteProducts}
//                     cartProducts={cartProducts}
//                 />)
//                 }
//             </div>
//             <ProductModalAddToCart
//                 onClose={handleModal}
//                 isOpen={isModal}
//                 id={currentProduct.id}
//                 name={currentProduct.name}
//                 picture={currentProduct.picture}
//                 onCart={() => {
//                     onCart(currentProduct)
//                 }
//                 }
//                 cartProducts={cartProducts}
//             />
//         </>
//     );
// };

// export default CartPage;


import React, { useState } from 'react';
import Product from "../../components/Products/components/Product/Product.jsx";
import ProductModalAddToCart from "../../components/Products/components/ProductModals/ProductModalAddToCart.jsx";

import "./CardPage.scss"

const CartPage = ({ cartProducts, handleCurrentProduct, currentProduct, favoriteProducts, onFavorite, onCart }) => {

    const [isModal, setIsModal] = useState(false)
    const handleModal = () => setIsModal(!isModal)

    return (
        <>
            <div className="cart-products-wrapper">
                {cartProducts?.map((cartProduct) => <Product
                    key={cartProduct.id}
                    id={cartProduct.id}
                    name={cartProduct.name}
                    price={cartProduct.price}
                    picture={cartProduct.picture}
                    color={cartProduct.color}
                    linkPath={`/product/${cartProduct.id}`}
                    onClick={() => {
                        handleCurrentProduct(cartProduct)
                        handleModal()
                    }
                    }
                    onFavorite={() => {
                        onFavorite(cartProduct)
                    }
                    }
                    favoriteProducts={favoriteProducts}
                    cartProducts={cartProducts}
                />)
                }
            </div>
            <ProductModalAddToCart
                onClose={handleModal}
                isOpen={isModal}
                id={currentProduct.id}
                name={currentProduct.name}
                picture={currentProduct.picture}
                onCart={() => {
                    onCart(currentProduct)
                }
                }
                cartProducts={cartProducts}
            />
        </>
    );
};

export default CartPage;