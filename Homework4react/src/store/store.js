import { combineReducers, configureStore } from "@reduxjs/toolkit"
import favoriteReducer from "./slices/favoriteSlice.js"
import cartReducer from "./slices/cardSlise.js"
import modalReducer from "./slices/modalSlice.js"
import productsReducer from "./slices/productsSlice.js"

const rootReducer = combineReducers({
    favorites: favoriteReducer,
    cart: cartReducer,
    modal: modalReducer,
    getAllProducts: productsReducer,
})

export const store = configureStore({
    reducer: rootReducer
})