import { createSlice } from "@reduxjs/toolkit";

const localStorageFavorites = JSON.parse(localStorage.getItem('favoriteProducts')) === null ? [] : JSON.parse(localStorage.getItem('favoriteProducts'))

const initialState = {
    favoriteProducts: localStorageFavorites,
}

const favoritesSlice = createSlice({
    name: "favoritesSlice",
    initialState,
    reducers: {
        toggleFavorites: (state, { payload }) => {
            let isProductAdded = state.favoriteProducts.some(({ id }) => id === payload.id)
            if (!isProductAdded) {
                state.favoriteProducts = [...state.favoriteProducts, payload]
                console.log("slice state.favoriteProducts", state.favoriteProducts);
                localStorage.setItem('favoriteProducts', JSON.stringify(state.favoriteProducts));
            } else {
                state.favoriteProducts = state.favoriteProducts.filter((item) => item.id !== payload.id)
                localStorage.setItem('favoriteProducts', JSON.stringify(state.favoriteProducts));
            }
        }
    }
})

export const { toggleFavorites: toggleFavoritesAction } = favoritesSlice.actions
export default favoritesSlice.reducer