import { createSlice } from "@reduxjs/toolkit"

const localStorageCart = JSON.parse(localStorage.getItem('cartProducts')) === null ? [] : JSON.parse(localStorage.getItem('cartProducts'))

const initialState = {
    cartProducts: localStorageCart,
}

const cartSlice = createSlice({
    name: "cartSlice",
    initialState,
    reducers: {
        toggleCartProducts: (state, { payload }) => {
            let isAdded = state.cartProducts.some(({ id }) => id === payload.id)
            if (!isAdded) {
                state.cartProducts = [...state.cartProducts, payload]
                localStorage.setItem('cartProducts', JSON.stringify(state.cartProducts));
            } else {
                state.cartProducts = state.cartProducts.filter((item) => item.id !== payload.id)
                localStorage.setItem('cartProducts', JSON.stringify(state.cartProducts));
            }
        }
    }
})

export const { toggleCartProducts: toggleCartProductsAction } = cartSlice.actions
export default cartSlice.reducer