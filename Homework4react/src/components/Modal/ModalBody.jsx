import React from 'react';
import PropTypes from "prop-types";

const ModalBody = ({picture, name}) => {
    return (
            <div className="modal-body">
                <img src={picture} alt={name}/>
                <p>{name}</p>
            </div>
    );
};

ModalBody.propTypes = {
    picture: PropTypes.string,
    name: PropTypes.string,
}

export default ModalBody;