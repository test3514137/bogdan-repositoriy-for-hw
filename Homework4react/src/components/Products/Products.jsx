import React, { useState, useEffect } from 'react';

import Product from "./components/Product/Product.jsx"
import ProductModalAddToCart from "./components/ProductModals/ProductModalAddToCart.jsx";
import { sendRequest } from "../../helpers/sendRequest.js";

import "./Products.scss"
import PropTypes from "prop-types";

const Products = ({ onFavorite, onCart, favoriteProducts, cartProducts, currentProduct, handleCurrentProduct }) => {

    const [products, setProducts] = useState([]);
    const [isModal, setIsModal] = useState(false);

    

    const handleModal = () => setIsModal(!isModal)

    useEffect(() => {
        sendRequest('/products.json')
            .then((data) => setProducts(data))
    }, [])

    return (
        <>
            <main className="main-content">
                <div className="products-wrapper">
                    {products?.map((product) => <Product
                        key={product.id}
                        id={product.id}
                        name={product.name}
                        price={product.price}
                        picture={product.picture}
                        color={product.color}
                        linkPath={`/product/${product.id}`}
                        onClick={() => {
                            handleCurrentProduct(product)
                            handleModal()
                        }
                        }
                        onFavorite={() => {
                            onFavorite(product)
                        }
                        }
                        favoriteProducts={favoriteProducts}
                        cartProducts={cartProducts}
                    />)
                    }
                </div>
            </main>
            <ProductModalAddToCart
                onClose={handleModal}
                isOpen={isModal}
                id={currentProduct.id}
                name={currentProduct.name}
                picture={currentProduct.picture}
                onCart={() => {
                    onCart(currentProduct)
                }
                }
                cartProducts={cartProducts}
            />
        </>
    );
};

Products.propTypes = {
    onCart: PropTypes.func,
    onFavorite: PropTypes.func,

}

export default Products;