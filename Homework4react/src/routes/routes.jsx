import { BrowserRouter as Router, Routes } from "react-router-dom"
import { Route } from "react-router-dom"
import Homepage from "../pages/HomePage/Homepage.jsx";
import NotPage from "../pages/NotPage/NotPage.jsx";
import CartPage from "../pages/CardPage/CardPage.jsx";
import FavoritesPage from "../pages/FavoritePage/FavoritePage.jsx";
import ProductPage from "../pages/ProductPage/ProductPage.jsx";



export default ({ onFavorite, onCart, cartProducts, favoriteProducts, currentProduct, handleCurrentProduct, handleCartProduct }) => {
    return (
        
            <Routes>
                <Route path={"/"} element={<Homepage onFavorite={onFavorite} onCart={onCart} favoriteProducts={favoriteProducts} cartProducts={cartProducts} currentProduct={currentProduct} handleCurrentProduct={handleCurrentProduct} handleCartProduct={handleCartProduct} />} />
                <Route path={"/cart"} element={<CartPage onFavorite={onFavorite} onCart={onCart} favoriteProducts={favoriteProducts} cartProducts={cartProducts} currentProduct={currentProduct} handleCurrentProduct={handleCurrentProduct} handleCartProduct={handleCartProduct} />} />
                <Route path={"/wishlist"} element={<FavoritesPage onFavorite={onFavorite} onCart={onCart} favoriteProducts={favoriteProducts} cartProducts={cartProducts} currentProduct={currentProduct} handleCurrentProduct={handleCurrentProduct} handleCartProduct={handleCartProduct} />} />
                <Route path={"/product/:id"} element={<ProductPage />} />
                <Route path={"*"} element={<NotPage />} />
            </Routes>
        
    )
}