const makeRequest = (url) => {
    return new Promise((res, rej) => {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                res(response);
            } else if (xhr.readyState === 4) {
                rej(xhr.status);
            }
        };
        xhr.open("GET", url, true);
        xhr.send();
    });
};

const displayFilmsAndCharacters = async () => {
    try {
        const films = await makeRequest("https://ajax.test-danit.com/api/swapi/films");
        const filmsContainer = document.getElementById("films");

        films.forEach(async (film) => {
            const filmInfo = document.createElement("div");
            filmInfo.innerHTML = `<h2>Episode ${film.episodeId}: ${film.name}</h2><p>${film.openingCrawl}</p>`;
            filmsContainer.appendChild(filmInfo);

            const characters = await Promise.all(film.characters.map(makeRequest));
            const charactersList = document.createElement("ul");
            characters.forEach((character) => {
                const characterItem = document.createElement("li");
                characterItem.textContent = character.name;
                charactersList.appendChild(characterItem);
            });
            filmInfo.appendChild(charactersList);
        });
    } catch (error) {
        console.error("Error:", error);
    }
};

displayFilmsAndCharacters();