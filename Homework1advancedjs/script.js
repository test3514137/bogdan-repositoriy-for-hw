class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }


    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }


    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}


const programmer1 = new Programmer('Pasha', 30, 10000, ['JavaScript', 'Python']);
const programmer2 = new Programmer('Bohdan', 25, 20000, ['Java', 'C++']);

console.log(programmer1);
console.log(programmer2);
