class Card {
    constructor(post) {
        this.post = post;
        this.element = this.createCardElement();
    }

    createCardElement() {
        const card = document.createElement('div');
        card.classList.add('card');

        card.innerHTML = `
            <h2>${this.post.title}</h2>
            <p>${this.post.body}</p>
            <p>Author: ${this.post.user.name} ${this.post.user.surname} (${this.post.user.email})</p>
            <span class="delete-button" data-post-id="${this.post.id}">Delete</span>
        `;

        return card;
    }
}

async function fetchPosts() {
    const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
    const users = await usersResponse.json();

    const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
    const posts = await postsResponse.json();

    displayPosts(posts, users);
}

function displayPosts(posts, users) {
    const container = document.getElementById('posts-container');
    container.innerHTML = '';

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        if (user) {
            const card = new Card({
                id: post.id,
                title: post.title,
                body: post.body,
                user: {
                    name: user.name,
                    surname: user.surname,
                    email: user.email
                }
            });
            container.appendChild(card.element);
        }
    });
}

async function deletePost(postId) {
    const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE'
    });

    if (response.ok) {
        document.querySelector(`[data-post-id="${postId}"]`).closest('.card').remove();
    } else {
        console.error('Failed to delete post');
    }
}

document.addEventListener('DOMContentLoaded', () => {
    fetchPosts();

    document.getElementById('posts-container').addEventListener('click', event => {
        if (event.target.classList.contains('delete-button')) {
            const postId = event.target.getAttribute('data-post-id');
            deletePost(postId);
        }
    });
});
