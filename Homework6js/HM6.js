function craeteNewUser() {
    const firstName = prompt("Введіть ім'я:");
    const lastName = prompt("Введіть прізвище:");
    const birthday = prompt("Введіть дату народження(у форматі dd.mm.yyyy)");

    const newUser = {
        firstName,
        lastName,
        birthday,

        getLogin: function () {
            return `${this.firstName.toLocaleLowerCase().charAt(0)}${this.lastName.toLocaleLowerCase()}`
        },
        getAge: function () {
            let parts = this.birthday.split(".");
            let birthDate = new Date(parts[2], parts[1] - 1, parts[0]);
            let currentDate = new Date();
            let age = currentDate.getFullYear() - birthDate.getFullYear();
            if (
                currentDate.getMonth() < birthDate.getMonth() || (currentDate.getMonth() === birthDate.getMonth() && currentDate.getDate() < birthDate.getDate())
            ); else {
                // age--;

                return age;
            }
        },
        getPassword: function () {
            return `${this.firstName.toLocaleUpperCase().charAt(0)}${this.lastName.toLocaleLowerCase()}${this.birthday.charAt(6)}${this.birthday.charAt(7)}${this.birthday.charAt(8)}${this.birthday.charAt(9)}`
        }
    }

    return newUser;
}

let user = craeteNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());