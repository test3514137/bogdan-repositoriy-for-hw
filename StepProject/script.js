// Dinamic task 1
const tab = document.querySelectorAll('.tab');
const mainContent = document.querySelectorAll('.main-content');

tab.forEach(function (item) {
    item.addEventListener('click', function () {
        let currentTab = item;
        let tabId = currentTab.getAttribute('data-tab');
        let currentTabId = document.querySelector(tabId);

        tab.forEach(function (item) {
            item.classList.remove('active');
        })

        mainContent.forEach(function (item) {
            item.classList.remove('active');
        })

        currentTab.classList.add('active');
        currentTabId.classList.add('active');
    })
});



// Dinamic task 2
const loadMore = document.querySelector('.btn4');
let items = 12;

loadMore.addEventListener('click', (event) => {
    event.preventDefault()


    items += 12;
    const array = Array.from(document.querySelectorAll('.grid-img'));
    const activeItems = array.slice(0, items);

    activeItems.forEach(el => el.classList.add('active-items'));
    loadMore.remove();
});



// Dinamic task 3
const filterTab = document.querySelectorAll('.grid-img');
document.querySelector('.tabs3').addEventListener('click', event => {
    if (event.target.tagName !== 'DIV') return false;
    let filterClass = event.target.dataset['filter'];
    filterTab.forEach(elem => {
        elem.classList.remove('hide');
        if (!elem.classList.contains(filterClass) && filterClass !== 'All') {
            elem.classList.add('hide');
        }
    })
})
