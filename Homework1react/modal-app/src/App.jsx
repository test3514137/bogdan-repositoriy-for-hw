import React, { useState } from 'react';
import './App.css';
import Button from './Button';
import { ModalImage, ModalText } from './Modal';

function App() {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setSecondModalOpen(false);
  };

  return (
    <>
      <div>
        <Button onClick={openFirstModal}>Open first modal</Button>
        <Button onClick={openSecondModal}>Open second modal</Button>
      </div>

      {firstModalOpen && (
        <ModalImage
          imageUrl="https://img.freepik.com/free-photo/vivid-blurred-colorful-background_58702-2655.jpg"
          altText="First Modal"
          onClose={closeFirstModal}
        />
      )}
      {secondModalOpen && (
        <ModalText text="Second Modal Text" onClose={closeSecondModal} />
      )}
    </>
  );
}

export default App;