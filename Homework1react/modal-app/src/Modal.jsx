import React from 'react';
import "./styles.scss"
import Button from './Button';
const ModalWrapper = ({ children }) => {
    return <div className="modal-wrapper" onClick={(event) => {
        event.stopPropagation();
    }}>{children}
    
    </div>;
};

const ModalHeader = ({ children }) => {
    return <div className="modal-header">{children}</div>;
};

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => {
    return (
        <div className="modal-footer">
            {firstText && <button onClick={firstClick}>{firstText}</button>}
            {secondaryText && <button onClick={secondaryClick}>{secondaryText}</button>}
        </div>
    );
};

const ModalClose = ({ onClick }) => {
    return (
        <button className="modalClose" onClick={onClick}>
            &times;
        </button>
    );
};

const ModalBody = ({ children }) => {
    return <div className="modal-body">{children}</div>;
};

const Modal = ({ children, onClick }) => {
    return <div className="modal" onClick={onClick}>{children}</div>;
};

const ModalImage = ({ imageUrl, altText, onClose }) => {




    return (
        <Modal onClick={onClose}>
            <ModalWrapper>
                <ModalHeader>
                </ModalHeader>
                <ModalBody>
                    
                    <div className='modalWindow'>
                        <ModalClose className = 'modalClose' onClick={onClose}  />
                        <img className="imgModal" src={imageUrl} alt={altText} />
                        <h1 className='titleModal'>Product Delete!</h1>
                        <p className='textModal'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
                        <div className='buttons'>
                            <Button classNames={'modalButton1'} onClick={onClose}>NO, CANCEL</Button>
                            <Button classNames={'modalButton2'} onClick={onClose}>YES, DELETE</Button>
                        </div>
                    </div>
                </ModalBody>
            </ModalWrapper>
        </Modal>
    );
};

const ModalText = ({ text, onClose }) => {
    return (
        <Modal onClick={onClose}>
            <ModalWrapper>
                <ModalHeader>
                </ModalHeader>
                <ModalBody>
                    <div className='modalWindow2'>
                        <ModalClose onClick={onClose} />
                    <h2 className='titleModal2'>Add Product “NAME”</h2>
                        <p className='textModal'>Description for you product</p>
                        <Button classNames={'modalButton3'} onClick={onClose}>ADD TO FAVORITE</Button>
                    </div>
                </ModalBody>
            </ModalWrapper>
        </Modal>
    );
};

export { ModalWrapper, ModalHeader, ModalFooter, ModalClose, ModalBody, Modal, ModalImage, ModalText };