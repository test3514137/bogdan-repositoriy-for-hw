import React from 'react';
import ModalWrapper from "./ModalWrapper.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";
import ModalBox from "./ModalBox.jsx";

import ProductImage from "../../../public/images/grapefruit.jpg"
import PropTypes from "prop-types";

const ModalImage = ({close, isOpen}) => {

    return (
        <ModalWrapper close={close} isOpen={isOpen}>
            <ModalBox>
                <ModalClose close={close}/>
                <ModalHeader>
                    <img
                        src={ProductImage}
                        alt="Grapefruit slice" />
                    <h6>Product Delete</h6>
                </ModalHeader>
                <ModalBody>
                    <p>By clicking the "Yes, Delete" button, product name will be deleted.</p>
                </ModalBody>
                <ModalFooter
                    firstText = "No, cancel"
                    firstClick = {()=>{}}
                    secondaryText = "Yes, delete"
                    secondaryClick = {()=>{}}
                >
                </ModalFooter>
            </ModalBox>
        </ModalWrapper>
    );
};

ModalImage.propTypes = {
    close: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default ModalImage;