import React from 'react';

import "./Header.scss"
import CartIcon from "../ui/icons/CartIcon.jsx"
import FavoritesIcon from "../ui/icons/FavoritesIcon.jsx";
import MenuIcon from "../ui/icons/MenuIcon.jsx";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = ({ favoriteProductsCounter, cartProductsCounter }) => {
    return (
        <header className='header'>
            <Link to={"/"}>
                <div className='header-logo'>
                    <div className='header-logo-icon'>
                        <MenuIcon />
                    </div>
                </div>
            </Link>
            <div className="header-icons-wrapper">
                <Link to={"/wishlist"}>
                    <div className="header-icon">
                        <FavoritesIcon />
                        <span className="header-icon-counter">{favoriteProductsCounter}</span>
                    </div>
                </Link>
                <Link to={"/cart"}>
                    <div className="header-icon">
                        <CartIcon />
                        <span className="header-icon-counter">{cartProductsCounter}</span>
                    </div>
                </Link>
            </div>
        </header>
    );
};

Header.propTypes = {
    favoriteProductsCounter: PropTypes.number,
    cartProductsCounter: PropTypes.number,

}

export default Header;