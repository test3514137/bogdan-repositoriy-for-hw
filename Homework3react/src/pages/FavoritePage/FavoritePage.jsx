import React, { useState } from 'react';
import Product from "../../components/Products/components/Product/Product.jsx";

import "../FavoritePage/FavoritePage.jsx"
import ProductModalAddToCart from "../../components/Products/components/ProductModals/ProductModalAddToCart.jsx";

const FavoritesPage = ({ favoriteProducts, cartProducts, currentProduct, handleCurrentProduct, onFavorite, onCart }) => {

    const [isModal, setIsModal] = useState(false)
    const handleModal = () => setIsModal(!isModal)

    return (
        <>
            <div className="wishlist-wrapper">
                {favoriteProducts?.map((favoriteProduct) => <Product
                    key={favoriteProduct.id}
                    id={favoriteProduct.id}
                    name={favoriteProduct.name}
                    price={favoriteProduct.price}
                    picture={favoriteProduct.picture}
                    color={favoriteProduct.color}
                    linkPath={`/product/${favoriteProduct.id}`}
                    onClick={() => {
                        handleCurrentProduct(favoriteProduct)
                        handleModal()
                    }
                    }
                    onFavorite={() => {
                        onFavorite(favoriteProduct)
                    }
                    }
                    onCart={onCart}
                    favoriteProducts={favoriteProducts}
                    cartProducts={cartProducts}
                />)
                }
            </div>
            <ProductModalAddToCart
                onClose={handleModal}
                isOpen={isModal}
                id={currentProduct.id}
                name={currentProduct.name}
                picture={currentProduct.picture}
                onCart={() => {
                    onCart(currentProduct)
                }
                }
                cartProducts={cartProducts}
            />
        </>
    );
};

export default FavoritesPage;