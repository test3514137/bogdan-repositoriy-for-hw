document.getElementById('findIPBtn').addEventListener('click', async function () {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const ipAddress = ipData.ip;

        const locationResponse = await fetch(`https://ipapi.co/${ipAddress}/json/`);
        const locationData = await locationResponse.json();

        const resultDiv = document.getElementById('result');
        resultDiv.innerHTML = `
                    <p>Континент,: ${locationData.continent}</p>
                    <p>Країна: ${locationData.country}</p>
                    <p>Регіон: ${locationData.regionName}</p>
                    <p>Місто: ${locationData.city}</p>
                    <p>Район: ${locationData.district}</p>
                `;
    } catch (error) {
        alert('ПОМИЛКА!!!');
    }
});


// Не розумію чому undefind, робив з чатом gpt)